# Purple for the win!

This theme brings back the purple tab background to make the new super-clean Firefox version just a tiny bit more colourful. And if you're a night owl, it also provides you with the regular dark mode.

![Preview](https://codeberg.org/pixelcode/purple-for-the-win/raw/branch/master/preview.png)

## Download

Download the "Purple for the win!" theme: [https://addons.mozilla.org/firefox/addon/purple-for-the-win](https://addons.mozilla.org/firefox/addon/purple-for-the-win)

## Licence

This theme is licensed under the [MIT Licence](https://codeberg.org/pixelcode/purple-for-the-win/raw/branch/master/LICENCE.md).